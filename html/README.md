# Magento Community Edition

Magento Community Magento-1.x Mirror Github Repository with Patches.

## Installation via Composer

If you do not know what Composer is, please first read [this](https://getcomposer.org/doc/00-intro.md).

To generate `composer.json` and install magento for the first time run:

```
composer require rockar/magento-ce-core ~1.9.2.4
```
